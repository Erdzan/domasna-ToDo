<!DOCTYPE html>
<html>
<head>
	<title>To Do List App</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
	<div class="col-md-offset-2 col-md-8">
		<div class="row">
			<h1>To Do List</h1>
		</div>

		<div class="row">
			<form method="POST" action="{{route('insert')}}">
			{{ csrf_field() }}

				<div class="col-md-6">
					<input type="text" name="name" class="form-control">
				</div>

				<div class="col-md-3">
					<input type="date" name="data" class="form-control">
				</div>

				<div class="col-md-3">
					<input type="submit" class="btn btn-primary btn-block" value="Add Task">
				</div>
				
			</form>
		</div>
		@if (count($storedTasks) > 0)
			<table class="table">
				<thead>
					<th>Task #</th>
					<th>Name</th>
					<th>Date</th>
					<th>Edit</th>
					<th>Delete</th>
				</thead>
				<tbody>
					@foreach($storedTasks as $storedTask)
					<tr>
						<th>{{ $storedTask->id }}</th>
						<td>{{ $storedTask->name }}</td>
						<td>{{ $storedTask->date }}</td>
						<td>Edit</td>
						<td>
							<form method="POST" action="{{route('delete',[$storedTask->id]) }}">
								{{ csrf_field()}}
								{{method_field('DELETE')}}
								<input type="submit" class="btn btn-danger" value="Delete">
							</form>
							
						</td>
						<br>
					</tr>
					@endforeach

				</tbody>
			</table>

		@endif

	</div>
</div>

</body>
</html>