<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/form')
->name('display-form')
->uses('todoController@ShowForm');

Route::post('/insert')
->name('insert')
->uses('todoController@insertToDo');

Route::post('/delete')
->name('delete')
->uses('todoController@deleteTodo');

Route::delete('/delete/{id}', 'todoController@deleteTodo')->name('delete');